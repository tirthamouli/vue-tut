new window.Vue({
  el: "#exercise",
  data: {
    value: 0,
    time: "undefined",
    sec: 5
  },
  computed: {
    result: function() {
      return this.value >= 37 ? "done" : "not there yet";
    }
  },
  watch: {
    value: function(value) {
      if (this.value >= 37) {
        if (this.time !== "undefined") {
          clearTimeout(this.time);
        }
        this.time = setTimeout(() => {
          this.value = 0;
        }, this.sec * 1000);
      }
    }
  }
});
