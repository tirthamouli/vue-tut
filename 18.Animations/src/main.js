import Vue from "vue";
import App from "./App.vue";
import "materialize-css/dist/css/materialize.min.css";
import "materialize-css/dist/js/materialize.min.js";
import "animate.css";

new Vue({
  el: "#app",
  render: h => h(App)
});
