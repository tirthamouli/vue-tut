import Vue from "vue";
import App from "./App.vue";
import Home from "./components/Home.vue";

Vue.component("Server", Home);

new Vue({
  el: "#app",
  // Root Component. (data has to be a function)
  render: h => h(App)
});
