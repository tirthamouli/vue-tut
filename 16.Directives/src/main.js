import Vue from "vue";
import App from "./App.vue";
import "materialize-css/dist/css/materialize.min.css";
import "materialize-css/dist/js/materialize.min.js";

Vue.directive("highlight", {
  // bind hook
  bind(el, binding, vnode) {
    let delay = 0;
    if (binding.modifiers.delayed) {
      delay = 1000;
    }

    setTimeout(() => {
      if (binding.arg === "background") {
        // el.style.backgroundColor = "green";
        el.style.backgroundColor = binding.value;
      } else {
        el.style.color = binding.value;
      }

      if (binding.modifiers.bold) {
        el.style.fontWeight = "bold";
      }
    }, delay);
  }
});

new Vue({
  el: "#app",
  render: h => h(App)
});
