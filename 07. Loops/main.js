new window.Vue({
  el: "#app",
  data: {
    ingredients: ["meat", "fruit", "vegetables"],
    persons: [
      { id: 1, name: "Max", age: 27, color: "red" },
      { id: 2, name: "Jake", age: 30, color: "blue" },
      { id: 3, name: "Shaun", age: 30, color: "coral" }
    ]
  }
});
