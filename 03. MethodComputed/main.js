new window.Vue({
  el: "#app",
  data: { counter: 0, secondCounter: 0, time: "undefined" },
  // Computed only runs when required data property changes
  computed: {
    output() {
      console.log("computed");
      return this.counter > 5 ? "Greater than 5" : "Less than 5";
    }
  },
  // Methods run everytime data is updated
  methods: {
    result() {
      console.log("method");
      return this.counter > 5 ? "Greater than 5" : "Less than 5";
    }
  },
  // Async tasks performed here
  watch: {
    // Match property name with property in data
    counter: function(value) {
      if (value >= 5) {
        if (this.time !== "undefined") {
          clearTimeout(this.time);
        }
        this.time = setTimeout(() => {
          this.counter = 0;
        }, 2000);
      }
    }
  }
});
