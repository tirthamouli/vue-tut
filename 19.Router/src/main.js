import Vue from 'vue'
import App from './App.vue'
import 'materialize-css/dist/css/materialize.min.css'
import 'materialize-css/dist/js/materialize.min.js'
import router from './router/index'

router.beforeEach((to, from, next) => {
  console.log('Global before each')
  next()
})

/* eslint-disable */
new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
