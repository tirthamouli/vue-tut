import Vue from 'vue'
import Router from 'vue-router'
import About from '../components/About.vue'
import Contact from '../components/Contact.vue'
import Home from '../components/Home.vue'

// Lazy loading.
const User = resolve => {
  require.ensure(['../components/User.vue'], () => {
    resolve(require('../components/User.vue'))
  }, 'User') // User is group name
}

const UserList = resolve => {
  require.ensure(['../components/UserList.vue'], () => {
    resolve(require('../components/UserList.vue'))
  }, 'User')
}

const UserDetails = resolve => {
  require.ensure(['../components/UserDetails.vue'], () => {
    resolve(require('../components/UserDetails.vue'))
  }, 'User')
}

const UserEdit = resolve => {
  require.ensure(['../components/UserEdit.vue'], () => {
    resolve(require('../components/UserEdit.vue'))
  }, 'User')
}

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/about',
      name: 'About',
      component: About
    },
    {
      path: '/contact',
      name: 'Contact',
      component: Contact
    },
    {
      path: '/user',
      name: 'User',
      component: User,
      children: [
        {
          path: '',
          name: 'UserList',
          component: UserList
        },
        {
          path: ':id',
          name: 'UserDetails',
          component: UserDetails,
          beforeEnter (to, from, next) {
            console.log('Inside rotute setup')
            next()
          }
        },
        {
          path: ':id/edit',
          name: 'UserEdit',
          component: UserEdit
        },
        {
          path: '/redirect',
          name: 'Redirect',
          redirect: { name: 'Home' }
        },
        {
          path: '/*',
          redirect: { name: 'Home' }
        }
      ]
    }
  ],
  mode: 'history',
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    }
    if (to.hash) {
      return { selector: to.hash }
    }
    return { x: 0, y: 0 }
  }
})
