new window.Vue({
  el: "#app1",
  data: {
    title: "Hello World"
  },
  methods: {
    changeTitle: function(event) {
      this.title = event.target.value;
    }
  }
});

new window.Vue({
  el: "#app2",
  data: {
    title: "Hello Again"
  }
});
