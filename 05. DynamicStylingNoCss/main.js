new window.Vue({
  el: "#app",
  data: { color: "coral", width: 100 },
  computed: {
    style: function() {
      return {
        backgroundColor: this.color,
        width: this.width + "px"
      };
    }
  }
});
