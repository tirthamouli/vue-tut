new window.Vue({
  el: "#exercise",
  data: {
    effect: "none",
    blue: "blue",
    green: "green",
    orange: "orange",
    coral: "coral",
    className: "class",
    isBig: false,
    color: "black",
    width: 0,
    time: false
  },
  computed: {
    style() {
      return {
        backgroundColor: this.color,
        width: 100 + "px",
        height: 100 + "px"
      };
    },
    progress() {
      return {
        backgroundColor: "purple",
        height: 20 + "px",
        width: this.width + "%"
      };
    }
  },
  methods: {
    startEffect: function() {
      let vm = this;
      setInterval(function() {
        vm.effect = vm.effect === "highlight" ? "shrink" : "highlight";
      }, 1000);
    },
    startProgress() {
      this.time = setInterval(() => {
        this.width += 1;
        if (this.width === 100) {
          clearInterval(this.time);
          this.width = 0;
        }
      }, 2);
    }
  }
});
