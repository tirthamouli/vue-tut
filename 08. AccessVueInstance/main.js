window.Vue.component("hello", {
  template: "<h1>Hello Component</h1>"
});

var vm1 = new window.Vue({
  data: {
    title: "Hello World"
  },
  methods: {
    changeTitle: function(event) {
      this.title = event.target.value;
    }
  }
});

vm1.$mount("#app1");

setTimeout(() => {
  vm1.title = "Coool";
  vm1.$refs.head.innerText = "WOW";
}, 3000);

new window.Vue({
  el: "#app2",
  data: {
    title: "Hello Again"
  },
  methods: {
    change(event) {
      vm1.changeTitle(event);
    }
  }
});

var vm3 = new window.Vue({
  template: "<h1>HELLO</h1>"
});

vm3.$mount("#app3");

// document.getElementById("app3").appendChild(vm3.$el);
