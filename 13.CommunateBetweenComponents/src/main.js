import Vue from "vue";
import App from "./App.vue";

export const eventBus = new Vue({
  methods: {
    resetAge(age) {
      this.$emit("resetAge", age);
    }
  }
});

new Vue({
  el: "#app",
  render: h => h(App)
});
