new window.Vue({
  el: "#app",
  data: {
    ingredients: ["meat", "fruit", "vegetables"],
    persons: [
      { id: 1, name: "Max", age: 27, color: "red" },
      { id: 2, name: "Jake", age: 30, color: "blue" },
      { id: 3, name: "Shaun", age: 30, color: "coral" }
    ]
  },
  beforeCreate() {
    console.log("before create");
  },
  created() {
    console.log("created");
  },
  beforeMount() {
    console.log("before mount");
  },
  mounted() {
    console.log("mounted");
  },
  beforeUpdate() {
    console.log("before update");
  },
  updated() {
    console.log("updated");
  },
  beforeDestroy() {
    console.log("before destroy,");
  },
  destroyed() {
    console.log("destroyed");
  }
});
