new window.Vue({
  el: "#app",
  data: {
    started: false,
    log: [],
    health: {
      player: 100,
      monster: 100
    },
    charge: 1
  },
  methods: {
    startGame() {
      this.health.player = 100;
      this.health.monster = 100;
      this.started = true;
      this.charge = 1;
      this.log = [];
    },
    reset() {
      this.health.player = 100;
      this.health.monster = 100;
      this.started = false;
      this.log = [];
    },
    checkIfDead(who) {
      if (who === "player") {
        if (this.health.player <= 0) {
          this.reset();
          alert("You lost!! Monster won");
        }
      } else {
        if (this.health.monster <= 0) {
          this.reset();
          alert("Congrats!! You won!! Monster lost");
        }
      }
    },
    monsterTurn() {
      // Setting a random value of attack and attack type
      let monsterAttackType = Math.floor(Math.random() * 2);
      let monsterAttackValue = Math.random() * 30;

      // 0 is attack type else heal
      if (monsterAttackType === 0) {
        this.health.player -= monsterAttackValue;
        this.log.unshift({
          by: "monster",
          type: "attack",
          value: monsterAttackValue
        });
        this.checkIfDead("player");
      } else {
        this.health.monster =
          this.health.monster + monsterAttackValue > 100
            ? 100
            : this.health.monster + monsterAttackValue;
        this.log.unshift({
          by: "monster",
          type: "heal",
          value: monsterAttackValue
        });
      }
    },
    chargeSpecial() {
      this.charge += this.charge;
    },
    attack() {
      // Setting a random value of attack
      let playerAttackValue = Math.random() * 20;

      this.health.monster -= playerAttackValue;
      this.log.unshift({
        by: "player",
        type: "attack",
        value: playerAttackValue
      });
      this.checkIfDead("monster");
      this.monsterTurn();
      this.chargeSpecial();
    },
    heal() {
      // Setting a random value of attack
      let playerAttackValue = Math.random() * 20;

      this.health.player =
        this.health.player + playerAttackValue > 100
          ? 100
          : this.health.player + playerAttackValue;
      this.log.unshift({
        by: "player",
        type: "heal",
        value: playerAttackValue
      });
      this.monsterTurn();
      this.chargeSpecial();
    },
    special() {
      // Setting a random value of attack
      let playerAttackValue = Math.random() * this.charge;

      this.health.monster -= playerAttackValue;
      this.log.unshift({
        by: "player",
        type: "special attack",
        value: playerAttackValue
      });
      this.checkIfDead("monster");
      this.monsterTurn();
      this.charge = 1;
    }
  }
});
