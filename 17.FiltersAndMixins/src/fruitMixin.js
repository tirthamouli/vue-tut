export const fruitMixin = {
  data() {
    return {
      filterFruit: "",
      fruits: ["Apple", "Banana", "Mango", "Melon"]
    };
  },
  computed: {
    filteredFruits() {
      return this.fruits.filter(fruit => {
        return fruit.match(this.filterFruit);
      });
    }
  },
  created() {
    console.log("Created");
  }
};
