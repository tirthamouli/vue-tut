import Vue from "vue";
import App from "./App.vue";
import "materialize-css/dist/css/materialize.min.css";
import "materialize-css/dist/js/materialize.min.js";

Vue.filter("reverse", value => {
  return value
    .split("")
    .reverse()
    .join("");
});

Vue.mixin({
  created() {
    console.log("Global mixin created hook");
  }
});

new Vue({
  el: "#app",
  render: h => h(App)
});
