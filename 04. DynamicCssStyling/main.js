new window.Vue({
  el: "#app",
  data: { attachRed: false, color: "green" },
  computed: {
    attachClass: function() {
      return { red: this.attachRed, blue: !this.attachRed };
    }
  }
});
