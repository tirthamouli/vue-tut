new window.Vue({
  el: "#exercise",
  data: {
    value: ""
  },
  methods: {
    alertMe: function() {
      alert("Hello");
    },
    handleChange: function(event) {
      this.value = event.target.value;
    }
  }
});
