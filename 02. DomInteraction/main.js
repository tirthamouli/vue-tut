new window.Vue({
  el: "#app",
  data: {
    title: "Hello World!",
    link: "https://google.com",
    finishedLink:
      "<a href='https://tirthamouli-library.herokuapp.com/'>Library</a>",
    counter: 0,
    x: 0,
    y: 0,
    name: "Boom"
  },
  methods: {
    // has to be something that can be converted to a string to be rendered output in the DOM
    sayHello: function() {
      this.title = "Hello!";
      return this.title;
    },
    updateCoordinate: function(event) {
      this.x = event.clientX;
      this.y = event.clientY;
    },
    stopP: function(event) {
      event.stopPropagation();
    },
    alertMe: function(event) {
      alert("Hello");
    }
  }
});
