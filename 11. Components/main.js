// Globally registered component
window.Vue.component("my-cmp", {
  data() {
    return {
      status: "critical"
    };
  },
  template:
    "<p>Server Status {{ status }} <button @click= 'changeStatus'>Change</button></p>",
  methods: {
    changeStatus() {
      this.status = "normal";
    }
  }
});

var cmp = {
  data() {
    return {
      status: "critical"
    };
  },
  template:
    "<p>Server Status {{ status }} <button @click= 'changeStatus'>Change</button></p>",
  methods: {
    changeStatus() {
      this.status = "normal";
    }
  }
};

new window.Vue({
  el: "#app"
});

new window.Vue({
  el: "#app2",
  //Local component
  components: {
    pell: cmp
  }
});
