const state = {
  counter: 0
}

const getters = {
  doubleCounter (state) {
    return state.counter * 2
  },
  stringCounter (state) {
    return state.counter + ' clicks'
  }
}

const mutations = {
  increment (state, payload = 1) {
    state.counter += payload
  },
  decrement (state, payload = 1) {
    state.counter -= payload
  }
}

const actions = {
  increment (context, payload) {
    context.commit('increment', payload)
  },
  decrement (context, payload) {
    context.commit('decrement', payload)
  },
  asyncIncrement (context) {
    setTimeout(() => {
      context.commit('increment')
    }, 1000)
  },
  asyncDecrement (context) {
    setTimeout(() => {
      context.commit('decrement')
    }, 1000)
  }
}

export default{
  state,
  getters,
  mutations,
  actions
}
