##### install them for linting

npm i -g babel-eslint eslint eslint-config-prettier eslint-config-react-app eslint-plugin-flowtype eslint-plugin-import eslint-plugin-jsx-a11y eslint-plugin-prettier eslint-plugin-react prettier eslint-config-vue-app eslint-plugin-markdown

# Vue JS

## What is Vue JS

It is a front end framework to create JavaScript driven webpages

## Why Vue JS

1. It is a very lean framework (16kB minified and gzipped)
2. It has great performance

##### It is extremely fast

3. It is also very feature rich even though it is a small frame work

## Vue Instance

It is an object used to control the element using 'el' property.
It is like the middle man between our DOM and the business logic(all our business logic is packed inside the Vue instance)
VueJS proxies the properties from the data, method, computed field to the main object

### Properties in the vue instance

1. \$el: It refers to out template(Native html element)
2. \$data: Contains the data block which is passed into the view instance
3. \$ref: Access simple html elements which is inside the Vue controll by adding a ref attribute
4. \$mount: Mount a template. Not use the el prop and mount it externally

### How Vue uses templates

1. Takes the html code that we provide
2. Stores it internally as a javascript Object(Quicker to run through js than the native DOM)
3. Uses this template and uses string interpolation, etc and updates the DOM only when required. i.e. Doesn't update the DOM for some property that is not reflected in the DOM in any way

#### VueJS doesn't create an enclosed world. It is able to interact with the javaScript code around it

## Vue directive

Special command that Vue Js recognizes.
It is an instruction that you place in your code
Can write your own directive

### Directives:

1. v-bind:<argument> : Binds something to my data (used in cases where we can use double curlies)
2. v-on:<event>: Binds event
3. v-once: Render only once and no re-render
4. v-html: Output html instead of plain text. VueJS by default prevents XSS attacks
5. v-model: Sets up 2 way data binding
6. v-if: bind to a property which resolves to true or false to render something to the DOM (Completely removes from the DOM or attaches it)
7. v-show: show or hide an element. Element will be present in the DOM in either case
8. v-for: loop through an array of elements

#### Event modifier used to modify behaviour of events

v-on:<event>.<modifier>.<another modifier>
.stop: stopPropagation()
.prevent: preventDefault()
.lazy: Added to v-model, this modifier updates the data lazily, i.e. only when focus is lost
.trim: Removes white space
.number forces to input number

#### Key event modifiers

.enter: when enter is pressed
.space: when spacebar is pressed

## Method vs Computed

1. Method is called as a function whereas computed is called as a property
2. Method gets called everytime data changes and computed only gets called when required data is changed

## Watch object:

Property of Vue object to execute code on data change, async tasks done here

##### Best practices to use computed. Use computed only for syncronous tasks

## Shorthands

1.'v-on:' : '@'
2.'v-bind:': ':'

## Template property

Sets the current template as given in the template

### Limitations to template property

1. Harder to write multi-line
2. Syntax highlighting doesn't work

### Limitation of both el and template

1. Need to respect native DOM properties

# Two versions of VueJS

1. One with built in compiler. Supports templates both el and template property
2. Without a compiler. Pre-compile during build process. So when we ship our app, we only have compiled JS code when we need to render the DOM. Pre-compiled version is smaller and faster

## Virtual DOM

1. Vue Instance creates a virtual DOM from the instance and renders the DOM from that
2. On any change Virtual DOM is updated and the current Virtual DOM is compared to the old Virtual DOM and the difference is updated in the real DOM.

##### Similar to React.JS, this makes Vue really fast becuase JavaScript is very fast. It is time consuming to update the real DOM

## Life cycle methods

1. beforeCreate(): Before Vue instance is created
2. created(): After data and events are initialized and the Vue Insance is created
3. beforeMount():Compiles the template from el or template property
4. mounted(): After mounted
5. beforeUpdate()
6. updated()
7. beforeDestroyed()
8. destroyed()

# WebPack and VueCli

## Why we need a Vue CLI and such a workflow?

1. Have a buid tool: Gives us a production version with ES5 uglified and mininfied code
2. A development server: Test under more realistic circumstances, lazy loading

## What is a development workflow?

Helps ship better production version code and significantly reduce the size of VueJS code => 30% size reduction

### What build does

1. Compile single file templates: Powerful alternative to using the el property or template property
2. Case-insentitive component selector:
3. Pre-processor and more: babel for ES6 to ES5

# Vue Components

Vue.component(<element>,{<options>})

## Why data is a method that returns an object in a component?

To create separate memory for all data objects and so that it doesn't interfere with the data of the root object

DOM is case insensitive

## Scoping styles

Scoped styles emulates the behaviour of shadow DOM
Shadow DOM is next gen HTML behaviour where every element has its DOM. or the main DOM has many sub DOMs

# Event Bus

It is a central state manger which is also a Vue instance only used to emit and listen to events. Use Vuex which is more similar to Redux

# Slot

It is a part of the parent element

1. Styling of parent applies
2. Parent element does the string interpolation

# Dynamic component

<component :is="<component name in string>"></component>: Used to set components dynamically
<keep-alive></keep-alive>: is used to keep the dynamic components alive when navigated away from it

## Life cycle hook

1. activated: for when the dynamic component is activated
2. deactivated: for when the dynamic component is deactivated

## What v-model does?

1. v-bind:value binds the value
2. v-on:change sets event handler

# Custom Directive

## 5 hooks of a directive

1. bind(el, binding, vnode) : fired once element is bound to the element
   a. el: element where the directive sits on
   b. binding(read-only): refers to the way this directive is set up
   c. vnode(read-only): node in the virtual DOM

2. inserted(el, binding, vnode): as soon as element is inserted into the DOM

3. update(el, binding, vnode, oldVNode): once component updates. Children not updated yet

4. componentUpdated(el, binding, vnode, oldVNode): once both the component and the children have been updated

5. unbind: once the directive is removed

#### bind and update used most often

# Filters

A filter is a feature we can use to transform the output in the template. It doesn't transform the data itself. but only changes what the user sees

VueJS doens't have built in filters

#### Use computed more than filters

# Mixins

Using external JS file to store the object of data etc and later use them as mixins in different components

1. Mixins are merged and nothing is destroyed. Component data is always kept
2. Lifecycle hooks in both the mixin and the component gets executed. First of the mixin and then of the component. Hence the lifecycle hook of the component is able to overwrite any changes that it wants to in the end

## Global mixin

Added to every instance and evry component in the App.

Order
Global mixin -> Local mixin -> Component

##### Data is not shared between different components using the same mixin

# Animation

## <traninstion name=<name>></transition>

Put elements inside this tag to animate

1. Can have only single element or only one element inside the transition can be displayed at a time

When we add transition tags VueJS does this:

1. Add <name>-enter class first and adds <name>-enter-active and removes <name>-enter while entering
   Similar thing is done while leaving

### attributes

1. type: for setting which animation type should determine how long the animation occurs
2. appear: animate when user lands on the page as well

# transition-group for multiple items

# Router

1. vue-router to route
   <router-view> is where routed components are shown
   <router-link to:"<link>"> for links

# Vuex (Similar to redux)

## Centralized state management: 1 file which stores the application state

### Use getter to get state from vuex after performing any required function. Similar to subscribing in redux

### Use mutations for changing the state

### Mutations are syncronous
